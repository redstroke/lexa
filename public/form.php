<html>
  <head>
    <style>
/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
.error {
  border: 2px solid red;
}
    </style>
  </head>
  <body>

<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>

    <form action="" method="POST">
          <p><strong>Имя</strong></p>
      <input name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>" />
      <p><strong>Почта</strong></p>
  <input name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" />
  <p><strong>Год</strong></p>
   <select name="year" <?php if ($errors['year']) {print 'class="error"';} ?> value="<?php print $values['year']; ?>" />
    <?php for($i = 1900; $i < 2020; $i++) { ?>
    <option value="<?php print $i; ?>"  <?php if ($errors['year']) {print 'class="error"';} ?> <?php if ($i === $values['year']){print 'checked="checked"';}  ?> ><?= $i; ?></option>
    <?php } ?>
  </select><br>
   <p><strong>Пол</strong></p>
            <p><input type = "radio" name = "sex" value = "F"  <?php if ($errors['sex']) {print 'class="error"';} ?> <?php if ('F' === $values['sex']){print 'checked="checked"';}  ?>>Ж
            <input type = "radio" name = "sex" value = "M"  <?php if ($errors['sex']) {print 'class="error"';} ?> <?php if ('M' === $values['sex']){print 'checked="checked"';}  ?>>М
            <input type = "radio" name = "sex" value = "П"  <?php if ($errors['sex']) {print 'class="error"';} ?> <?php if ('П' === $values['sex']){print 'checked="checked"';}  ?>>Паркет</p>
            <p><strong>Сколько конечностей?</strong></p>
            <p><input type = "radio" name = "legs" value = "1"  <?php if ($errors['legs']) {print 'class="error"';} ?> <?php if ('1' === $values['legs']){print 'checked="checked"';}  ?>>1
            <input type = "radio" name = "legs" value = "2"  <?php if ($errors['legs']) {print 'class="error"';} ?> <?php if ('2' === $values['legs']){print 'checked="checked"';}  ?>>2
            <input type = "radio" name = "legs" value = "3"  <?php if ($errors['legs']) {print 'class="error"';} ?> <?php if ('3' === $values['legs']){print 'checked="checked"';}  ?>>3
            <input type = "radio" name = "legs" value = "4"  <?php if ($errors['legs']) {print 'class="error"';} ?> <?php if ('4' === $values['legs']){print 'checked="checked"';}  ?>>4
            <input type = "radio" name = "legs" value = "more"  <?php if ($errors['legs']) {print 'class="error"';} ?> <?php if ('more' === $values['legs']){print 'checked="checked"';}  ?>>больше</p>
            <p><strong>Сверхспособности</strong></p>
  <select name="abilities[]" multiple>
  <?php 
  foreach ($ability_labels as $key => $value) {
  ?>
    <option value="<?= $key; ?>"<?php if ($errors['abilities']) {print 'class="error"';} ?>  <?php if ($key === $values['abilities']){print 'checked="checked"';}  ?> ><?= $value; ?></option>
  <?php } ?>
  </select>
  <p><strong>Биография</strong></p>
            <p><textarea name = "biography"  cols = "50" rows = "10" <?php if ($errors['biography']) {print 'class="error"';} ?>><?php print $values['biography']; ?></textarea></p>
  <input type="checkbox" name="check1" checked="checked" <?php if ($errors['check1']) {print 'class="error"';} ?> <?php if ('1' === $values['check1']){print 'checked="checked"';}  ?>/> С контрактом ознакомлен

      
      
      <input type="submit" value="ok" />
    </form>

  </body>
</html>